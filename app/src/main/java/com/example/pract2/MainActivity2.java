package com.example.pract2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.pract2.databinding.ActivityMain2Binding;

public class MainActivity2 extends AppCompatActivity {

    ActivityMain2Binding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle arguments = getIntent().getExtras();
        binding.editText2.setText(arguments.get("data").toString());

    }
    public void Close(View view) {
        Intent intent = new Intent();
        intent.putExtra("data",binding.editText2.getText());
        setResult(0,intent);
        finish();
    }
}